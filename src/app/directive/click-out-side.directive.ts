
/*
 * Handle click outside event
 * @Author: Zhou Fang
 * @Date: 2017-09-05 14:55:50
 * @Last Modified by: Zhou Fang
 * @Last Modified time: 2018-04-11 12:40:47
 */

import {
  Directive,
  ElementRef,
  Output,
  EventEmitter,
  HostListener,
  Input,
  AfterViewInit,
  OnInit
} from '@angular/core';
/**
 * @description
 * @export
 * @class ClickOutSideDirective
 * @Output() public clickOutside = new EventEmitter();
 * @Input() public exceptionDom?: ElementRef;
 * @Input() public useFlag = true; this flag to break the current zone;For example if you want to use *ngIf to toggle the component
 * @implements {OnInit}
 * @implements {AfterViewInit}
 */
@Directive({
  selector: '[appClickOutSide]'
})
export class ClickOutSideDirective implements OnInit, AfterViewInit {
  @Output() public clickOutside = new EventEmitter();
  @Input() public exceptionDom?: ElementRef;
  @Input() public useFlag = true;

  /**
   * this flag to break the current zone;For example if you want to use *ngIf to toggle the component
   * @type {boolean}
   * @memberof ClickOutSideDirective
   */
  public flag = true;

  constructor(private _el: ElementRef) {}

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    if (this.flag) {
      // check click inside
      const clickedInside = this._el.nativeElement.contains(targetElement);
      let clickExceptionDom;
      // check click exception dom
      if (this.exceptionDom) {
        clickExceptionDom = this.exceptionDom.nativeElement.contains(
          targetElement
        );
      } else {
        clickExceptionDom = false;
      }
      // emit the output event
      if (!clickedInside && !clickExceptionDom) {
        this.clickOutside.emit();
      }
    }
  }

  public updateFlag() {
    if (this.useFlag) {
      setTimeout(() => {
        this.flag = true;
      }, 0);
    }
  }

  public ngOnInit() {
    if (this.useFlag) {
      this.flag = false;
    }
  }

  public ngAfterViewInit() {
    this.updateFlag();
   // console.log(this.exceptionDom);
  }
}
