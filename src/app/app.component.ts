import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('exception', { read: ElementRef })
  public exceptionDOM;
  title = 'app';
  show = true;
  callFunctionText = '()=>{alert(`click outside,close the Inside div`)}';
  toggleShow() {
    this.show = !this.show;
  }
  trigger() {
    const callFunction = eval(this.callFunctionText);
    this.toggleShow();
    callFunction();
  }
  ngAfterViewInit() {
    //  console.log(this.exceptionDOM);
  }
}
