**angular-click-outside-directive**

https://bitbucket.org/zhou_fang_/angular-click-outside-directive

 * @class ClickOutSideDirective
 * @Output() public clickOutside = new EventEmitter();
 * @Input() public exceptionDom?: ElementRef;
 * @Input() public useFlag = true; this flag to break the current zone;For example if you want to use *ngIf to toggle the component
 * @implements {OnInit}
 * @implements {AfterViewInit}

---
